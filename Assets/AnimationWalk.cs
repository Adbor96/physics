﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AnimationWalk : MonoBehaviour
{
    private Animator anim;
    private Camera cam;
    private bool isWalking;
    private bool isRunning;
    [SerializeField]
    private AudioClip[] stoneFootSteps;
    [SerializeField]
    private AudioClip[] snowFootSteps;
    [SerializeField]
    private AudioClip[] grassFootSteps;
    private AudioSource source;

    [SerializeField]
    private float speed;
    private float turnSmoothTime = 0.1f;
    float turnSmoothVelocity;

    private TerrainDetector terrainDetector;

    private float x;
    private float z;
    private CharacterController controller;

    // Start is called before the first frame update
    void Start()
    {
        cam = GetComponentInChildren<Camera>();
        anim = GetComponent<Animator>();
        source = GetComponent<AudioSource>();
        terrainDetector = new TerrainDetector();
        controller = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        x = Input.GetAxis("Horizontal");
        z = Input.GetAxis("Vertical");

        Vector3 move = new Vector3(x, 0, z);

        move = Vector3.ClampMagnitude(move, 1); //rör sig inte snabbare diagonalt
        if(move.magnitude >= 0.1f)
        {
            //vrider spelaren mot hållet den går
            float targetAngle = Mathf.Atan2(move.x, move.z) * Mathf.Rad2Deg + cam.transform.eulerAngles.y; 
            //smoothar att spelaren vrider sig
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
            transform.rotation = Quaternion.Euler(0, angle, 0);

            Vector3 moveDir = Quaternion.Euler(0, targetAngle, 0) * Vector3.forward;
            controller.Move(moveDir.normalized * speed * Time.deltaTime);
        }


        if(controller.velocity.magnitude > 0)
        {
            isWalking = true;
        }
        else
        {
            isWalking = false;
        }

        anim.SetBool("Walking", isWalking);
        anim.SetBool("Running", isRunning);
        if (Input.GetKeyDown(KeyCode.Space))
        {
            isWalking = !isWalking;
            Debug.Log("walk");
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (isWalking)
            {
                isRunning = !isRunning;
            }
        }
    }
    public void Step()
    {
        source.clip = GetRandomClip();
        source.Play();
    }
    private AudioClip GetRandomClip()
    {
        int terrainTextureIndex = terrainDetector.GetActiveTerrainTextureIdx(transform.position);

        switch (terrainTextureIndex)
        {
            case 0:
                return stoneFootSteps[UnityEngine.Random.Range(0, stoneFootSteps.Length)];
            case 1:
                return grassFootSteps[UnityEngine.Random.Range(0, grassFootSteps.Length)];
            case 2:
            default:
                return snowFootSteps[UnityEngine.Random.Range(0, snowFootSteps.Length)];
        }
    }
}
