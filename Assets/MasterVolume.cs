﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class MasterVolume : MonoBehaviour
{
    public AudioMixer mixer;
    [SerializeField]
    private Slider _slider;
    // Start is called before the first frame update
    void Start()
    {
        _slider.value = PlayerPrefs.GetFloat("MasterVolume", 0.75f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void changeVolume()
    {
        float sliderValue = _slider.value;
        mixer.SetFloat("MasterVolume", sliderValue);
        PlayerPrefs.SetFloat("MasterVolume", sliderValue);
    }
}
