﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

    public Sound[] sounds;
    public Sound[] footStepSounds;

    public AudioMixerGroup soundEffects;
    public AudioMixerGroup voiceEffects;
    public AudioMixerGroup music;
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else if(instance != null)
        {
            Destroy(this);
        }
        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
        }

        foreach(Sound f in footStepSounds)
        {
            f.source = gameObject.AddComponent<AudioSource>();
            f.source.clip = f.clip;

            f.source.volume = f.volume;
            f.source.pitch = f.pitch;
        }
    }
    
    public void PlaySound(string name, float pitchMin, float pitchMax)
    {
        Sound s = Array.Find(sounds, Sound => Sound.name == name); //hitta det sound i sound arrayen som har samma namn som det ljud vi letar efter
        if(s == null)
        {
            Debug.LogWarning("Sound " + name + " not found.");
            return;
        }
        s.source.outputAudioMixerGroup = soundEffects;
        s.source.pitch = UnityEngine.Random.Range(pitchMin, pitchMax);
        s.source.Play();
    }
    public void StopSound(string name)
    {
        Sound s = Array.Find(sounds, Sound => Sound.name == name); //hitta det sound i sound arrayen som har samma namn som det ljud vi letar efter
        s.source.Stop();
    }
    public void PlayRandomFootstep(float pitchMin, float pitchMax)
    {
        Sound f = footStepSounds[UnityEngine.Random.Range(0, footStepSounds.Length)];
        f.source.pitch = UnityEngine.Random.Range(pitchMin, pitchMax);
        f.source.outputAudioMixerGroup = soundEffects;
        f.source.Play();
        Debug.Log("Sound");

    }
    public void PlayVoiceEffect(string name, float pitchMin, float pitchMax)
    {
        Sound s = Array.Find(sounds, Sound => Sound.name == name); //hitta det sound i sound arrayen som har samma namn som det ljud vi letar efter
        if (s == null)
        {
            Debug.LogWarning("Sound " + name + " not found.");
            return;
        }
        s.source.outputAudioMixerGroup = voiceEffects;
        s.source.pitch = UnityEngine.Random.Range(pitchMin, pitchMax);
        s.source.Play();
    }
}
