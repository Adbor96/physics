﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootStepSounds : MonoBehaviour
{
    public AudioClip[] footSounds;
    private AudioSource source;
    // Start is called before the first frame update
    void Start()
    {
        source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void PlayRandomSound()
    {
        //source = gameObject.AddComponent<AudioSource>();
        source.clip = footSounds[Random.Range(0, footSounds.Length)];
        source.Play();
    }
    public void Stop()
    {
        source.Stop();
    }
}
