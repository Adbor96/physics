﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class VolumeSlider : MonoBehaviour
{
    public AudioMixer mixer;

    private prefs _prefs;

    private Slider masterSlider;
    private Slider voiceSlider;
    private Slider effectsSlider;
    private Slider musicSlider;
    // Start is called before the first frame update
    void Start()
    {
        _prefs = FindObjectOfType<prefs>();
        masterSlider = transform.GetChild(0).GetComponent<Slider>();
        voiceSlider = transform.GetChild(1).GetComponent<Slider>();
        effectsSlider = transform.GetChild(2).GetComponent<Slider>();
        musicSlider = transform.GetChild(3).GetComponent<Slider>();
        //masterSlider.value = PlayerPrefs.GetFloat("MasterVolume", 0.75f);
      /*  voiceSlider.value = PlayerPrefs.GetFloat("VoiceVolume", 0.75f);
        effectsSlider.value = PlayerPrefs.GetFloat("EffectsVolume", 0.75f);
        musicSlider.value = PlayerPrefs.GetFloat("MusicVolume", 0.75f);*/

    }
    private void Update()
    {
       /* //mixer.SetFloat("MasterVolume", Mathf.Log10(masterSlider.value) * 20);
        //PlayerPrefs.SetFloat("MasterVolume", masterSlider.value);
        mixer.SetFloat("VoiceVolume", Mathf.Log10(voiceSlider.value) * 20);
        PlayerPrefs.SetFloat("VoiceVolume", voiceSlider.value);
        mixer.SetFloat("EffectsVolume", Mathf.Log10(effectsSlider.value) * 20);
        PlayerPrefs.SetFloat("EffectsVolume", effectsSlider.value);
        mixer.SetFloat("MusicVolume", Mathf.Log10(musicSlider.value) * 20);
        PlayerPrefs.SetFloat("MusicVolume", musicSlider.value);*/
    }
    public void SetMasterVolume(float sliderValue)
    {
        Debug.Log("Change volume");
        mixer.SetFloat("MasterVolume", Mathf.Log10(masterSlider.value) * 20);
        PlayerPrefs.SetFloat("MasterVolume", masterSlider.value);
    }
    public void SetVoiceVolume(float value)
    {
        Debug.Log("Change volume");
        mixer.SetFloat("VoiceVolume", Mathf.Log10(value) * 20);
        PlayerPrefs.SetFloat("VoiceVolume", Mathf.Log10(value) * 20);
    }
    public void SetEffectsVolume(float value)
    {
        Debug.Log("Change volume");
        mixer.SetFloat("EffectsVolume", Mathf.Log10(value) * 20);
        PlayerPrefs.SetFloat("EffectsVolume", Mathf.Log10(value) * 20);
    }
    public void SetMusicVolume(float value)
    {
        Debug.Log("Change volume");
        mixer.SetFloat("MusicVolume", Mathf.Log10(value) * 20);
        PlayerPrefs.SetFloat("MusicVolume", Mathf.Log10(value) * 20);
    }
}
