﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterEnemy : MonoBehaviour
{
    public int enemyStatus;
    [SerializeField] private float speed;
    [SerializeField] private float maxY;
    [SerializeField] private float minY;

    [SerializeField] private LayerMask playerMask;

    Vector3 targetPosition;
    private bool checkPosition;

    [SerializeField] private GameObject missilePrefab;
    [SerializeField] private Transform missileExit;
    private bool hasFired;

    private float attackRate = 2;
    private float nextTimeToAttack;
    // Start is called before the first frame update
    void Start()
    {
        enemyStatus = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (enemyStatus == 1)
        {
            transform.position += Vector3.left * speed * Time.deltaTime;
        }
        if(enemyStatus == 2)
        {
            if (!checkPosition)
            {
                targetPosition = GetRandomPosition();
            }
            if (transform.position != targetPosition)
            {
                Debug.Log("Move");
                transform.position = Vector2.MoveTowards(transform.position, targetPosition, 1.5f * Time.deltaTime);
            }
            else
            {
                checkPosition = false;
            }
            RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.left, 15, playerMask);

            if(hit.collider != null)
            {
                //Debug.Log("Found player");
                if (Time.time >= nextTimeToAttack)
                {
                        Fire();
                        nextTimeToAttack = Time.time + 1 / attackRate;
                }
            }
        }
    }
    Vector2 GetRandomPosition()
    {
        float randomY = Random.Range(minY, maxY);
        checkPosition = true;
        return new Vector2(transform.position.x, randomY);
    }
    public void Die()
    {
        Destroy(gameObject);
    }

    void Fire()
    {
        GameObject missile = Instantiate(missilePrefab, missileExit.position, Quaternion.identity);
        hasFired = false;
    }
}
