﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterMissile : MonoBehaviour
{
    private float force;
    private Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        force += 0.05f;
    }
    private void FixedUpdate()
    {
        rb.AddForce(Vector2.right * force);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        ShooterEnemy enemy = collision.gameObject.GetComponent<ShooterEnemy>();
        if(enemy != null)
        {
            enemy.Die();
        }
        Destroy(gameObject);
    }
}
