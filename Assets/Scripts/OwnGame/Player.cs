﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private float forcePush;
    [SerializeField] private float speed;
    private Rigidbody2D rb;

    private bool goDown;
    private bool goUp;
    private float vertical;

    //shoot
    [SerializeField] private Transform missilePrefab;
    private bool hasFired;
    [SerializeField] private Transform exit;
    private float missileForce;

    //respawn
    private Vector2 startPos;
    private float _speed;
    private float _forcePush;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        startPos = transform.position;
        _speed = speed;
        _forcePush = forcePush;
        Respawn();
    }

    // Update is called once per frame
    void Update()
    {

         vertical = Input.GetAxis("Vertical");

        if (Input.GetKeyDown(KeyCode.W))
        {
            goUp = true;
        }

        if(Input.GetKeyDown(KeyCode.S))
        {
            goDown = true;
        }

        //shoot
        if (Input.GetMouseButtonDown(0))
        {
            hasFired = true;
        }
    }
    private void FixedUpdate()
    {
        if (goUp)
        {
            rb.velocity = Vector2.up * forcePush;
            goUp = false;
        }


        if (goDown)
        {
            rb.velocity = Vector2.down * forcePush;
            goDown = false;
        }
        
        //shoot
        if (hasFired)
        {
            Transform missile = Instantiate(missilePrefab, exit.position, Quaternion.identity);
            hasFired = false;
        }
    }
    void Respawn()
    {
        GetComponent<BoxCollider2D>().enabled = true;
        GetComponent<SpriteRenderer>().enabled = true;
        transform.position = startPos;
        forcePush = _forcePush;
    }
    void Die()
    {
        GetComponent<BoxCollider2D>().enabled = false;
        GetComponent<SpriteRenderer>().enabled = false;
        rb.velocity = new Vector2(0, 0);
        forcePush = 0;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag != "Bounds")
        {
            Debug.Log("Collision");
            Die();
        }

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        ShooterEnemy enemy = collision.gameObject.GetComponent<ShooterEnemy>();
        if (enemy != null)
        {
            enemy.enemyStatus = 2;
        }
    }
}
