﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsCubes : MonoBehaviour
{
    public GameObject physicsCube;
    public Color[] colors;
    public Transform instantiateSpot;
    // Start is called before the first frame update
    void Start()
    {
        /*colors[1] = Color.red;
        colors[2] = Color.blue;
        colors[3] = Color.green;
        colors[4] = Color.yellow;
        colors[5] = Color.cyan;
        colors[6] = Color.magenta;
        colors[7] = Color.white;
        colors[8] = Color.blue;
        colors[9] = Color.gray;
        colors[10] = Color.clear;*/

        for (int y = 0; y < colors.Length; y++)
        {
            GameObject newCube = Instantiate(physicsCube, new Vector3(instantiateSpot.position.x, y - 0.5f, 0), Quaternion.identity);
            newCube.GetComponent<MeshRenderer>().material.color = colors[y];
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
