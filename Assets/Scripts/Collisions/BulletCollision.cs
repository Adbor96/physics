﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCollision : MonoBehaviour
{
    float lifetime;
    // Start is called before the first frame update
    void Start()
    {
        lifetime = 5;
    }

    // Update is called once per frame
    void Update()
    {
        lifetime -= Time.deltaTime;
        if (lifetime <= 0)
            Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Dummy")
        {
            Destroy(collision.gameObject);
        }
        Destroy(gameObject);
    }
}
