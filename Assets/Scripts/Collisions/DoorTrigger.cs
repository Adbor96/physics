﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorTrigger : MonoBehaviour
{
    private DoorRotate door;
    // Start is called before the first frame update
    void Start()
    {
        door = GetComponentInChildren<DoorRotate>(); 
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        other.gameObject.GetComponent<KeyPickup>();
        if (other != null)
        {
            if (other.gameObject.GetComponent<KeyPickup>().canOpenDoor)
            {
                door.isOpening = true;
                door.isClosing = false;
            }
            else 
            {
                return;
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            door.isOpening = false;
            door.isClosing = true;
        }
    }
}
