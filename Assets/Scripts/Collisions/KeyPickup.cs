﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyPickup : MonoBehaviour
{
    public bool canOpenDoor;
    private PlayerPhysMovement playerMove;
    private bool isPoweredUp;
    float powerUpTimer;
    // Start is called before the first frame update
    void Start()
    {
        playerMove = GetComponent<PlayerPhysMovement>();
        powerUpTimer = 3;
    }

    // Update is called once per frame
    void Update()
    {
        if (isPoweredUp)
        {
            Powerup();
        }
    }
    void Powerup()
    {
        playerMove.speed = playerMove.doubleSpeed;
        playerMove.jumpForce = playerMove.increasedJumpForce;
        GetComponentInChildren<MeshRenderer>().material.color = Color.red;
        powerUpTimer -= Time.deltaTime;
        if (powerUpTimer <= 0)
        {
            PowerDown();
            powerUpTimer = 3;
        }
    }
    void PowerDown()
    {
        playerMove.speed = playerMove.defaultSpeed;
        playerMove.jumpForce = playerMove.defaultJumpForce;
        GetComponentInChildren<MeshRenderer>().material.color = Color.white;
        isPoweredUp = false;
    }
    private void OnCollisionEnter(Collision collision)
    {
        /*if (collision.gameObject.name == "Key")
        {
            Debug.Log("Key");
            canOpenDoor = true;
            Destroy(collision.gameObject);
        }*/
        switch (collision.gameObject.tag)
        {
            case "Key":
                canOpenDoor = true;
                Destroy(collision.gameObject);
                FindObjectOfType<AudioManager>().PlaySound("KeyPickup", 1, 1);
                break;

            case "Powerup":
                isPoweredUp = true;
                Destroy(collision.gameObject);
                break;

            default:
                break;
        }
    }
}
