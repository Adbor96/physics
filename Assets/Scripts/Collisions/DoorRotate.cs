﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorRotate : MonoBehaviour
{
    private Collider collider;
    private bool isOpen;
    private bool isClosed;
    public bool isOpening;
    public bool isClosing;
    [SerializeField]
    private float speed;
    // Start is called before the first frame update
    void Start()
    {
        collider = GetComponentInChildren<Collider>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isOpening)
        {
            Open();
        }
        if (isClosing)
        {
            Close();
        }
        

    }
    void Open()
    {
            Vector3 openVector = new Vector3(0, 10, 0);
            transform.Translate(openVector * Time.deltaTime);
            if (transform.position.y >= 10)
            {
            isOpening = false;
            }


    }
    void Close()
    {
        Debug.Log("Closing");
        Vector3 closeVector = new Vector3(0, -10, 0);
        transform.Translate(closeVector * Time.deltaTime);
            if (transform.position.y <= 0)
            {
            isClosing = false;
            }
    }
}
