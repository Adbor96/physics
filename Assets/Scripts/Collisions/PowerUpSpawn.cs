﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpSpawn : MonoBehaviour
{
    [SerializeField]
    private Transform powerupPrefab;
    [SerializeField]
    private Transform[] spawnpoints;

    [SerializeField]
    private GameObject powerup;
    private float spawnTimer;
    // Start is called before the first frame update
    void Start()
    {
        spawnTimer = 2;
    }

    // Update is called once per frame
    void Update()
    {
        powerup = GameObject.FindGameObjectWithTag("Powerup");
        if (powerup == null)
        {
            spawnTimer -= Time.deltaTime;
            if(spawnTimer <= 0)
            {
                int randomSpawnNumber = Random.Range(0, spawnpoints.Length);
                Transform randomSpawnPos = spawnpoints[randomSpawnNumber];
                Instantiate(powerupPrefab, randomSpawnPos.position, Quaternion.identity);
                spawnTimer = 2;
            }
            /*for (int i = 0; i < Random.Range(0, spawnpoints.Length - 1); i++)
            {
                Transform randomSpawnPos = spawnpoints[i];
                Instantiate(powerupPrefab, randomSpawnPos.position, Quaternion.identity);
            }*/
        }
    }
}
