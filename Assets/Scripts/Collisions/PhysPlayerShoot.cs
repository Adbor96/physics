﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysPlayerShoot : MonoBehaviour
{
    private bool hasFired;

    private bool singleShot;
    [SerializeField]
    private float force;

    private Transform exit;

    [SerializeField]
    private Transform bullet;

    bool rapidFireIsOn;

    float fireRate;
    float nextFireTime;

    // Start is called before the first frame update
    void Start()
    {
        exit = transform.Find("Exit");
        singleShot = true;
        fireRate = 3f;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if(singleShot)
            hasFired = true;

        }
        if (Input.GetMouseButton(0))
        {
            if (rapidFireIsOn)
            {
                if (Time.time >= nextFireTime)
                {
                    hasFired = true;
                }
            }
        }

        if (Input.GetMouseButtonDown(1))
        {
            rapidFireIsOn = !rapidFireIsOn;
            singleShot = !singleShot;
        }
        if (Input.GetMouseButtonUp(0))
        {
            if (rapidFireIsOn)
            {
                hasFired = false;
            }
        }
    }
    private void FixedUpdate()
    {
        if (hasFired && singleShot)
        {
            Transform bulletPrefab = Instantiate(bullet, exit.position, transform.rotation);
            Rigidbody bulletRb = bulletPrefab.GetComponent<Rigidbody>();
            bulletRb.AddForce(transform.forward * force);
            hasFired = false;
        }
        if (rapidFireIsOn)
        {
            if (hasFired)
            {
                Transform bulletPrefab = Instantiate(bullet, exit.position, transform.rotation);
                Rigidbody bulletRb = bulletPrefab.GetComponent<Rigidbody>();
                bulletRb.AddForce(transform.forward * force);
                nextFireTime = Time.time + 1f / fireRate;
                hasFired = false;
            }
        }
    }
    void Shoot()
    {

    }
}
