﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeCombat : MonoBehaviour
{

    [SerializeField] private Transform attackPoint; //där attacken kommer ifrån
    [SerializeField] private float attackRange; //hur långt attacken når
    [SerializeField] private LayerMask enemyLayer;
    [SerializeField] private float attackForce;

    private float attackRate = 2;
    private float nextTimeToAttack;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time >= nextTimeToAttack)
        {
            if (Input.GetKeyDown(KeyCode.V))
            {
                Attack();
                nextTimeToAttack = Time.time + 1 / attackRate;
            }
        }
    }
    void Attack()
    {
        //gör en attack animation (onödigt)

        //känn av närvarande fiender
            //skapa en array av träffade fiender utifrån hur många som känns av i en sfär utifrån där attacken kommer
        Collider[] enemiesHit = Physics.OverlapSphere(attackPoint.position, attackRange, enemyLayer);
        

        //skada de
            //gå igenom varje fiende som träffades i attackens kollider och skada de
        foreach(Collider enemy in enemiesHit)
        {
            Debug.Log("Träffade " + enemy.name);
            enemy.GetComponent<RayTarget>().TakeDamage(10);
            //knuffa fienden iväg från spelaren
            enemy.GetComponent<Rigidbody>().AddForce(transform.forward * attackForce + transform.up * attackForce);
            //enemiesHit.
        }
    }
    private void OnDrawGizmosSelected()
    {
        if (attackPoint == null)
        {
            return;
        }

        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
    }
}
