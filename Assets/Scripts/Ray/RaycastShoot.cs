﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastShoot : MonoBehaviour
{
    private float range = 100;
    private float damage = 10;
    [SerializeField] private float impactForce = 30;
    [SerializeField] private float fireRate = 15;
    private float nextTimeToFire;

    private Camera fpsCam;
    // Start is called before the first frame update
    void Start()
    {
        fpsCam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0) && Time.time >= nextTimeToFire)
        {
            Shoot();
            nextTimeToFire = Time.time + 1 / fireRate;
        }
    }
    void Shoot()
    {
        //information om vad som träffas med en ray
        RaycastHit hit;
        //vart strålen skickas från och dess riktning, och hur långt den når
         if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range))
        {
            //berätta vad som träffades
            Debug.Log(hit.transform.name);

            //skapar en variabel av fiender som träffas
            RayTarget target = hit.transform.GetComponent<RayTarget>();
            if(target != null)
            {
                target.TakeDamage(30);
            }

            //om det man träffar har en rigidbody
            if (hit.rigidbody != null)
            {
                //lägg till force bakåt
                hit.rigidbody.AddForce(-hit.normal * impactForce);
            }
        }
    }
}
