﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionManager : MonoBehaviour
{
    [SerializeField] private Material selectedMaterial;
    [SerializeField] private Material defaultMaterial;

    //tagg som markerar vilka objekt som ska kunna markeras
    [SerializeField] private string selecteableTag = "Selecteable";

    private Transform _selected;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(_selected != null)
        {
            var selectedRenderer = _selected.GetComponent<Renderer>();
            selectedRenderer.material = defaultMaterial;
            _selected = null;
        }

        //skapar en ray från kameran där musen är
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        //om strålen träffar något
        if (Physics.Raycast(ray, out hit))
        {
            //tar transform av det man träffat
            var selected = hit.transform;
            //om det man tittar på har rätt tag
            if (selected.CompareTag(selecteableTag))
            {
                var selectedRenderer = selected.GetComponent<Renderer>();
                if (selectedRenderer != null)
                {
                    //ändra färg på det objekt som valts
                    selectedRenderer.material = selectedMaterial;
                }
                _selected = selected;
            }
        }
    }
}
