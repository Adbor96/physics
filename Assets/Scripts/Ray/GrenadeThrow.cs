﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeThrow : MonoBehaviour
{
    [SerializeField] private float throwHeight;
    private float throwHeightDefault;
    [SerializeField] private float throwForce;
    private float throwForceDefault;
    private bool canThrow;
    private float throwRate = 1;
    private float timeToNextThrow;
    [SerializeField] private Transform grenadePrefab;
    [SerializeField] private Transform exit;
    // Start is called before the first frame update
    void Start()
    {
        throwForceDefault = throwForce; 
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time >= timeToNextThrow)
        {
            if (Input.GetMouseButton(2))
            {
                throwForce++;
            }
        }
        if (Time.time >= timeToNextThrow)
        {
            if (Input.GetMouseButtonUp(2))
            {
                Throw();
                throwForce = throwForceDefault;
                timeToNextThrow = Time.time + 1 / throwRate;
            }
        }
    }
    void Throw()
    {
        Transform grenade = Instantiate(grenadePrefab, exit.position, exit.rotation);
        Rigidbody rb = grenade.GetComponent<Rigidbody>();
        Vector3 throwDirectionUp = transform.up * throwHeight;
        Vector3 throwDirectionForward = transform.forward * throwForce;
        rb.AddForce(throwDirectionUp + throwDirectionForward);
    }
}
