﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : MonoBehaviour
{
    private float countdown = 3;
    [SerializeField] private float explosionForce;
    [SerializeField] private float radius;
    [SerializeField] private LayerMask enemyMask;
    private int damage = 40;
    private bool hasExploded = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        countdown -= 1 * Time.deltaTime;
        if(countdown <= 0 && !hasExploded)
        {
            Explode();
            hasExploded = true;
        }
    }
    void Explode()
    {
        Collider[] nearbyObjects = Physics.OverlapSphere(transform.position, radius);
        foreach(Collider objectHit in nearbyObjects)
        {
            Rigidbody nearbyRigidBodies = objectHit.GetComponent<Rigidbody>();
            if (nearbyRigidBodies != null)
            {
                nearbyRigidBodies.AddExplosionForce(explosionForce, transform.position, radius);
            }
            RayTarget enemy = objectHit.GetComponent<RayTarget>();
            if(enemy != null)
            {
                enemy.TakeDamage(damage);
            }
        }
        Destroy(gameObject);
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
