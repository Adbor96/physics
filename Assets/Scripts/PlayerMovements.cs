﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovements : MonoBehaviour
{
    public CharacterController controller;
    private AudioManager audioManager;

    private float x;
    private float z;

    public float speed = 12;
    private float gravity = -9.81f;
    private float jumpHeight = 3;
    Vector3 velocity;

    public Ray playerRay;
    public RaycastHit playerRayHit = new RaycastHit();

    private Transform groundCheck;
    float groundDistance = 0.4f;
    [SerializeField] private LayerMask groundMask;
    private bool isGrounded;
    private bool groundedLastframe;

    private float moveDistance; //hur långt spelaren har gått sedan den började röra sig
    [SerializeField]
    private float stepDistance; //hur långt spelaren måste röra sig innan ett fotsteg har tagits
    private AudioSource source;
    [SerializeField]
    private AudioClip[] footStepSounds;
    bool canPlayFootSound = true;

    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
        groundCheck = transform.GetChild(4);
        source = GetComponent<AudioSource>();
        audioManager = FindObjectOfType<AudioManager>();
    }

    // Update is called once per frame
    void Update()
    {
        x = Input.GetAxis("Horizontal");
        z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;

        if(isGrounded && x != 0 || isGrounded && z != 0)
        {
            //StartCoroutine(PlayFootStepSound());
        }
        if(isGrounded && x == 0 && z == 0)
        {
            //FindObjectOfType<FootStepSounds>().Stop();
        }

        groundedLastframe = isGrounded;
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
        Land();

        if (isGrounded && velocity.y <= 0) //makes sure the player is grounded by pushing him slightly down
        {
            velocity.y = -2;
        }

        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);

        move = Vector3.ClampMagnitude(move, 1); //rör sig inte snabbare diagonalt
        controller.Move(move * speed * Time.deltaTime);

       

        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            Jump();
        }
        if(controller.velocity.magnitude > 0 && isGrounded) //velocity ställs om till 0 av någon anledning
        {
            moveDistance += Time.deltaTime;
            if(moveDistance >= stepDistance)
            {
                StepSound();
            }
        }
        else if (controller.velocity.sqrMagnitude == 0)
        {
            //Debug.Log("Problem");
            //moveDistance = 0;
        }
        Debug.Log("Velocity = " + controller.velocity.magnitude);

        if (x != 0 || z != 0)
        {
            if (isGrounded)
            {
                moveDistance += Time.deltaTime;
                if(moveDistance >= stepDistance)
                {
                    //canPlayFootSound = true;
                   // StartCoroutine(PlayFootStepSoundTwo());
                }
                /*if (canPlayFootSound)
                {
                    StartCoroutine(PlayFootStepSoundTwo());
                }*/
            }
        }

        if (x != 0 || z != 0)
        {
            if (isGrounded)
            {
                if (canPlayFootSound)
                {
                    //StartCoroutine(PlayFootStepSoundTwo());
                }
            }
        }
    }
    void Jump()
    {

        audioManager.PlayVoiceEffect("PlayerJump", 0.8f, 1.2f);
        velocity.y = Mathf.Sqrt(jumpHeight * -2 * gravity);
    }

    void Land()
    {
        if (!groundedLastframe && isGrounded)
        {
            audioManager.PlaySound("PlayerLand", 1, 1);
        }
    }
    private IEnumerator PlayFootStepSound()
    {
        if (isGrounded && x != 0 || isGrounded && z != 0) //kollar om spelaren rörde sig och är på marken
        {
            AudioManager manager = FindObjectOfType<AudioManager>();
            //GetComponent<AudioSource>().pitch = Random.Range(1, 2);
            //GetComponent<FootStepSounds>().PlayRandomSound();
            manager.PlayRandomFootstep(1, 2);
            yield return new WaitForSeconds(0.4f);
        }
    }

    private IEnumerator PlayFootStepSoundTwo() //testar utan AudioManager
    {
        //if (isGrounded && x != 0 || isGrounded && z != 0) //kollar om spelaren rörde sig och är på marken
        //{
            source.clip = footStepSounds[Random.Range(0, footStepSounds.Length)];
           // if (!source.isPlaying)
               // {
                    //Debug.Log("Play sound");
                    source.Play();
            canPlayFootSound = false;
        moveDistance = 0;
                //}
                //Debug.Log("Stopping");
                yield return new WaitForSeconds(0.5f);
                //source.Stop();
        //canPlayFootSound = true;
                //Debug.Log("Stop");


        //}
    }
    private IEnumerator PlayFootStepSoundThree()
    {
        source.clip = footStepSounds[Random.Range(0, footStepSounds.Length)];

        //Debug.Log("Play sound");
        source.pitch = Random.Range(1, 1.3f);
            source.Play();
        canPlayFootSound = false;

        yield return new WaitForSeconds(.3f);

        canPlayFootSound = true;

    }
    void StepSound()
    {
        source.clip = footStepSounds[Random.Range(0, footStepSounds.Length)];
        source.Play();
        moveDistance = 0;
    }
}
