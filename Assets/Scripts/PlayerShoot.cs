﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    public GameObject sphere;
    public Transform exit;
    public Color[] colors;
    private float force;
    private float forceDefault;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(1))
        {
            force++;
        }
        if (Input.GetMouseButtonUp(1))
        {
            GameObject spherePrefab = Instantiate(sphere, exit.position, exit.rotation);
            Rigidbody sphereRb = spherePrefab.GetComponent<Rigidbody>();
            sphereRb.AddForce(transform.forward * force);
            spherePrefab.GetComponent<MeshRenderer>().material.color = colors[Random.Range(0, colors.Length)];
            force = 0;
        }
    }
}
