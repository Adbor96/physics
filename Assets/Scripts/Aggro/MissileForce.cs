﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileForce : MonoBehaviour
{
    public GameObject enemy;
    public float speed;
    public float force;
    private Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        enemy = GameObject.FindGameObjectWithTag("Enemy");
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("Force: " + force);
        //Debug.Log("Velocity: " + rb.velocity.normalized);
    }
    private void FixedUpdate()
    {
        Vector3 direction = enemy.transform.position - rb.position;
        //Debug.Log("Direction: " + direction);
        force += 0.05f;
        //rb.velocity = direction;
        direction.Normalize();
        rb.AddForce(direction * force);

    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Enemy")
        {
            Destroy(gameObject);
        }
    }
}
