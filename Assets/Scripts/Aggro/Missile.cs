﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : MonoBehaviour
{
    public GameObject missile;
    public Transform exit;
    public Transform enemy;
    public float speed;
    public float force;
    public bool hasFired;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            hasFired = true;
        }
    }
    private void FixedUpdate()
    {
        if (hasFired)
        {
            FindObjectOfType<AudioManager>().PlaySound("MissileFire", 1, 1);
            Instantiate(missile, exit.position, exit.rotation);
            hasFired = false;
        }
    }
}
