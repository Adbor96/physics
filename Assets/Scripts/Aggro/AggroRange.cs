﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AggroRange : MonoBehaviour
{
    private GameObject player;
    private float distanceToPlayer;
    private float speed;
    private Vector3 playerPosition;
    private bool chasingPlayer;
    private bool movedLastFrame;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        speed = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        playerPosition = player.transform.position;
        Vector3 distanceVector = player.transform.position - transform.position;
        distanceToPlayer = Vector3.Distance(transform.position, player.transform.position);
        Vector3 playerVector = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z);

        float playerDistanceLength = distanceVector.magnitude;

        //kan antingen använda Vector3.Distance eller en vector3 som kalkylerar avståndet och sedan använda dens längd (magnitud) som float
        if (playerDistanceLength <= 5 && !chasingPlayer)
        {
            chasingPlayer = true;
            FindObjectOfType<AudioManager>().PlaySound("EnemyDetection", 1, 1);
        }
        if (chasingPlayer)
        {
            Vector3 directionToPlayer = player.transform.position - transform.position;
            directionToPlayer.Normalize();
            transform.Translate(directionToPlayer * Time.deltaTime);
            //transform.position = Vector3.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);
            //movetowards: om avståndet till dit vi ska < hur långt vi flyttar på en update, snappas position dit du ska
            GetComponentInChildren<MeshRenderer>().material.color = Color.red;
            StartCoroutine(PlayFootStepSound());
        }
        if(playerDistanceLength >= 5 && chasingPlayer)
        {
            FindObjectOfType<AudioManager>().StopSound("EnemyDetection");
            chasingPlayer = false;
            GetComponentInChildren<MeshRenderer>().material.color = Color.white;
        }


    }
    private IEnumerator PlayFootStepSound()
    {
        if (!movedLastFrame) //kollar om fienden rörde sig senaste framen och är på marken
        {
            FindObjectOfType<AudioManager>().PlayRandomFootstep(0.2f, 0.5f);
            GetComponent<FootStepSounds>().PlayRandomSound();
            movedLastFrame = true;
            yield return new WaitForSeconds(0.4f);
            movedLastFrame = false;
        }
    }
}
