﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPhysMovement : MonoBehaviour
{
    private Rigidbody rb;
    private float moveX;
    private float moveZ;
    private float moveY;
    [SerializeField]
    public float jumpForce;
    public float defaultJumpForce;
    public float increasedJumpForce;

    private bool hasJumped;
    public float speed;
    public float defaultSpeed;

    private float stamina;
    private bool isRunning;
    public float doubleSpeed;

    private CapsuleCollider capCollider;
    [SerializeField]
    private LayerMask layerMask;

    [SerializeField] float radius;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        stamina = 100;
        defaultSpeed = speed;
        doubleSpeed = speed * 2;

        defaultJumpForce = jumpForce;
        increasedJumpForce = jumpForce * 2;

        capCollider = GetComponent<CapsuleCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        moveX = Input.GetAxis("Horizontal");
        moveZ = Input.GetAxis("Vertical");
        if (isGrounded() && Input.GetKeyDown(KeyCode.Space))
        {
            hasJumped = true;
        }

        if(stamina < 100 && !isRunning)
        {
            stamina += 5 * Time.deltaTime;
        }
        if(stamina >= 100 && !isRunning)
        {
            stamina = 100;
        }
        if (stamina <= 0)
        {
            stamina = 0;
        }
        if (Input.GetKey(KeyCode.LeftShift))
        {
            if (stamina > 0)
            {
                
                isRunning = true;
            }
            else
            {
                isRunning = false;
            }
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            isRunning = false;
            speed = defaultSpeed;
        }
        if (isRunning)
        {
            speed = doubleSpeed;
            stamina -= 50 * Time.deltaTime;
        }
        /*else if (!isRunning)
        {
            speed = defaultSpeed;
        }*/
    }
    private void FixedUpdate()
    {
        Vector3 moveDir = new Vector3(moveX, 0, moveZ);
        Vector3 moveTransform = transform.right * moveX + transform.forward * moveZ;

        //transform.localPosition += moveDir * speed;

        //transform.Translate(moveTransform * speed);
        transform.localPosition += moveTransform * speed;
        //transform.position += moveTransform * speed;
        //transform.localPosition += new Vector3(1 * moveX, 0, 0) + new Vector3(0, 0, 1 * moveZ);

        //rb.velocity = moveTransform * speed;

        //rb.transform.Translate(transform.right * moveX * speed);
        //rb.AddForce(transform.forward * moveZ * speed);
        //rb.AddForce(transform.right * moveX * speed);
        if (hasJumped)
        {
            //rb.velocity = transform.up * jumpForce;
            rb.velocity = Vector2.up * jumpForce;
            hasJumped = false;
        }
    }
    //bool om spelaren är på marken
    private bool isGrounded()
    {
        //en extra bit höjd för att vara säker att spelarens ray når marken
        float extraHeight = 0.1f;
        //en ray skickas ut från mitten av spelarens collider nedåt. rayen är lika lång som hälften av colliderns längd.
        //Physics.Raycast(collider.bounds.center, Vector3.down, collider.bounds.extents.y + extraHeight);
        Ray playerRay = new Ray(capCollider.bounds.center, Vector3.down);
        RaycastHit rayHit = new RaycastHit();

        //färg för ray
        Color rayColor;
        if(rayHit.collider != null)
        {
            rayColor = Color.blue;
            Debug.DrawRay(capCollider.bounds.center, Vector3.down * (capCollider.bounds.extents.y + extraHeight), rayColor);
        }
        else
        {
            rayColor = Color.red;
        }
        //om rayn träffar markens layermask
        if (Physics.Raycast(playerRay, out rayHit, capCollider.bounds.extents.y + extraHeight, layerMask))
        {
            return true;
        }
        //visa rayen i inspektorn, man måste multiplicera riktningen med längden
        Debug.DrawRay(capCollider.bounds.center, Vector3.down * (capCollider.bounds.extents.y + extraHeight), rayColor);

        return false;

        //osäker på hur capsuleCast kan användas här, gjorde tidigare så spelaren hela tiden var grounded

        /*if (Physics.CapsuleCast(capCollider.bounds.center, capCollider.bounds.extents, radius, Vector2.down, extraHeight, layerMask))
        {
            Debug.Log("Grounded");
            return true;
        }
        else
        {
            Debug.Log("Not grounded");
            return false;
        }*/
    }
}
