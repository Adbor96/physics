﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPhysLook : MonoBehaviour
{
    private Rigidbody rb;
    [SerializeField]
    private float mouseSensitivty;

    [SerializeField]
    private Transform player;

    float xRotation = 0;
    private float mouseX;
    [SerializeField]
    private float turnSpeed;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponentInParent<Rigidbody>();
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        mouseX = Input.GetAxis("Mouse X") * mouseSensitivty * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivty * Time.deltaTime;

        xRotation -= mouseY;
        if (xRotation > 90)
            xRotation = 90;

        if (xRotation < -90)
            xRotation = -90;

        transform.localRotation = Quaternion.Euler(xRotation, 0, 0);

        player.Rotate(Vector3.up * mouseX);

        //player.localRotation = Quaternion.Euler(0, mouseX, 0);
    }
    private void FixedUpdate()
    {
        //rb.AddTorque(xRotation * turnSpeed, xRotation * turnSpeed, 0);
    }
}
