﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPhysHealth : MonoBehaviour
{
    private int health;
    private int maxHealth;

    private Vector3 startPosition;
    // Start is called before the first frame update
    void Start()
    {
        maxHealth = 100;
        health = maxHealth;
        startPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Damage()
    {
        health--;
        if(health <= 0)
        {
            transform.position = startPosition;
            health = maxHealth;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.name == "Lava")
        {
            Damage();
        }
    }
}
